const test = require('ava');

const fs = require('fs');
const parseIndex = require('./parse/index-page');
const parseProduct = require('./parse/product-page');
const {newItems, newSizes, prepareReport} = require('./compare');

test('Parsing the items out of the HTML', (t) => {
  const html = fs.readFileSync('./fixtures/acr-2017-feb.html', 'utf8');

  t.deepEqual(parseIndex(html), [
    {name:"DAF1-001",price:"SOLD OUT",href:"/products/DAF1-001_NA"},
    {name:"DAF1-003",price:"SOLD OUT",href:"/products/DAF1-003_NA"},
    {name:"DAF1-006",price:"SOLD OUT",href:"/products/DAF1-006_NA"},
    {name:"IMG-15",price:"",href:"/images/IMG-15_SS15"},
    {name:"IMG-1516",price:"",href:"/images/IMG-1516_FW1516"},
    {name:"IMG-1617",price:"",href:"/images/IMG-1617_FW1617"},
    {name:"J40-L",price:"3,697.00 EUR",href:"/products/J40-L_FW1516"},
    {name:"J43-K",price:"SOLD OUT",href:"/products/J43-K_SS15"},
    {name:"J46-WS",price:"1,036.00 EUR",href:"/products/J46-WS_FW1617"},
    {name:"J50-S",price:"1,113.00 EUR",href:"/products/J50-S_FW1617"},
    {name:"J54-LP",price:"SOLD OUT",href:"/products/J54-LP_FW1516"},
    {name:"S10-C",price:"204.00 EUR",href:"/products/S10-C_SS15"},
    {name:"S18-BR",price:"417.00 EUR",href:"/products/S18-BR_FW1617"},
    {name:"S6-C",price:"352.00 EUR",href:"/products/S6-C_SS15"},
    {name:"S7-C",price:"399.00 EUR",href:"/products/S7-C_SS15"},
    {name:"S8-C",price:"179.00 EUR",href:"/products/S8-C_SS15"},
    {name:"S9-C",price:"189.00 EUR",href:"/products/S9-C_SS15"},
    {name:"V24-N",price:"",href:"/videos/V24-N_SS16"},
    {name:"V25-A",price:"",href:"/videos/V25-A_NA"}
  ]);
});

test('Parsing the sizes out of a product page', (t) => {
  const html = fs.readFileSync('./fixtures/acr-2017-J40L.html', 'utf8');
  t.deepEqual(parseProduct(html), ['Black / L']);
});

test('Parses no sizes out of a sold out product page', (t) => {
  const html = fs.readFileSync('./fixtures/acr-2017-DAF1.html', 'utf8');
  t.deepEqual(parseProduct(html), []);
});

test('newItems works', (t) => {
  const A = {name: 'a'};
  const B = {name: 'b'};
  // old is first arg, new is second arg
  t.deepEqual(newItems([A], [A, B]), [B]);
  t.deepEqual(newItems([A, B], [A, B]), []);
  t.deepEqual(newItems([], []), []);
  t.deepEqual(newItems([A, B], [A]), []);
});

// test fixtures
const S = 'S';
const M = 'M';
const L = 'L';
const XL = 'XL';

test('newSizes works', (t) => {
  // NOTE: old is first arg, new is second arg

  // a new item from an empty list does not count as a restock
  t.deepEqual(newSizes(
    [],
    [item('a', [S, M])]
  ), []);

  t.deepEqual(newSizes(
    [item('a', [S])],
    [item('a', [S, M])]
  ), [item('a', [M])]);

  // a new size added when an old size is removed
  t.deepEqual(newSizes(
    [item('a', [S])],
    [item('a', [M])]
  ), [item('a', [M])]);

  // empty lists
  t.deepEqual(newSizes([], []), []);

  // multiple items
  t.deepEqual(newSizes(
    [item('a', [S])],
    [item('a', [M])]
  ), [item('a', [M])]);
});

test('prepareReport works', (t) => {
  t.is(prepareReport([], []), '');

  t.is(prepareReport(
    [item('a', [S])],
    [item('a', [S])]
  ), '');

  t.is(prepareReport(
    [item('a', [S])],
    [item('a', [S, M])]
  ), 'RESTOCK:\na: M');

  t.is(prepareReport(
    [item('a', [S])],
    [item('b', [S, M])]
  ), 'NEW:\nb: S, M');

  t.is(prepareReport(
    [item('a', [S])],
    [item('a', [S, M]), item('b', [S])]
  ), 'NEW:\nb: S\nRESTOCK:\na: M');

  t.is(prepareReport(
    [],
    [item('a', [S, M, L, XL])]
  ), 'NEW:\na: 4 Sizes');
});

function item(name, sizes) {
  return {name, sizes};
}
