const cheerio = require('cheerio');

const ITEM_CSS_SELECTOR = 'a.tile';
const ITEM_NAME_SELECTOR = 'h2';
const ITEM_PRICE_SELECTOR = '.selected';

module.exports = function parseItems(html) {
  const $ = cheerio.load(html);

  const items = [];
  $(ITEM_CSS_SELECTOR).each((index, elt) => {
    const sub = $(elt);

    // for some reason, there are three empty items each time...
    if (sub.text().trim().length === 0) {
      console.log('yo');
      return;
    }
    const item = {
      name: sub.find(ITEM_NAME_SELECTOR).text().trim(),
      price: sub.find(ITEM_PRICE_SELECTOR).text().trim(),
      href: elt.attribs.href
    };
    items.push(item);
  });

  items.sort(function (a, b) {
    return a.name < b.name ? -1 : 1;
  });

  return items;
};
