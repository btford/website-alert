// This parses a product page to get sizes/colors

const cheerio = require('cheerio');

const SIZE_SELECTOR = '#variety_id option';

module.exports = function parseItems(html) {
  const $ = cheerio.load(html);

  const sizes = [];
  $(SIZE_SELECTOR).each((index, elt) => {
    sizes.push($(elt).text().trim());
  });

  return sizes;
};
