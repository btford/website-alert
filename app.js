// my number is 15407070627
// account SID: AC7d1cc2a8564df7583013c6865511c67f
// auth: ec514ab41549bb615e8e835f113a5e95
const http = require('http'),
      twilio = require('twilio'),
      axios = require('axios'),
      parseIndex = require('./parse/index-page'),
      parseProduct = require('./parse/product-page'),
      {newItems, newSizes, prepareReport} = require('./compare'),
      Table = require('cli-table'),
      mockGet = require('./mock-responses');

/**
 * Consts
 */
const DRY_MODE = process.argv.indexOf('--dry') > -1;
const MOCK_RESPONSES = process.argv.indexOf('--mock-responses') > -1;

const get = MOCK_RESPONSES ? mockGet : (url) => axios.get(url);

// every three minutes in miliseconds
const POLL_INTERVAL = (process.argv.indexOf('--fast-poll') > -1) ? 500 : (3 * 60 * 1000);

const TWILIO_NUMBER = '+15407070627';

const ADMIN_NUMBER = '+12693890430';

const ALERT_NUMBERS = [
  ADMIN_NUMBER,
  // '+14157604541', // Amy Martin

  // ACR SF crew
  '+13104608997', // Bryan Lee
  '+15103035698', // Flipflop
  '+14083348799', // Anthony Q
  // '+18319208081', // Christopher (24 fps)
  '+15104680659', // Kevin
  '+14087124944', // Mark
  // '+19165172250', // Casey
  '+17079804348', // Ed
  '+19496978150', // Eddie
  '+16504557130', // Gordon
  '+14083938880', // B.V
  '+18586689274', // Akeem
  '+18319208081', // Christopher DN
  '+16504385381'  // Karl O
];

// not sure why, but this page is fucked
const FUCKED_PAGES = [
  'S6-C'
];

function excudingFucked(items) {
  return items.filter(({name}) =>
    !FUCKED_PAGES.some(fucked =>
      name.toLowerCase().includes(fucked.toLowerCase())));
}

const SERVER_PORT = 1337;

/**
 * Setup app
 */

const client = twilio('AC7d1cc2a8564df7583013c6865511c67f', 'ec514ab41549bb615e8e835f113a5e95');

// start an HTTP server to host the XML blob for twilio
function startServer() {
  return http.createServer(function ({url}, res) {
    // parse out the item names
    const items = url.substr(url.indexOf('/') + 1).split(',');
    log('Creating TwiML for', items.join(', '));

    // Create TwiML response
    const twiml = new twilio.TwimlResponse();
    twiml.say(`${items.length} items were added to acrnm.com.`);
    items.forEach(item => twiml.say(`${item}.`));

    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end(twiml.toString());
  }).listen(SERVER_PORT);
}

function makeCall(phoneNumber, newItems) {
  if (DRY_MODE) {
    return log('DRY! CALLING', phoneNumber, newItems.join(', '));
  }
  //Place a phone call, and respond with TwiML instructions from the given URL
  client.makeCall({
    to: phoneNumber, // Any number Twilio can call
    // A number you bought from Twilio and can use for outbound communication
    from: TWILIO_NUMBER,
    // A URL that produces an XML document (TwiML) which contains instructions for the call
    url: `http://b4d.io:${SERVER_PORT}/${newItems.join(',')}`
  }, function(err, responseData) {
    //executed when the call has been initiated.
    //console.log(responseData.from); // outputs "+14506667788"
    log(`TW: CALL SENT TO ${responseData.to}`);
  });
}

function sendSms(phoneNumber, body) {
  if (DRY_MODE) {
    return log('DRY! TEXTING', phoneNumber, body);
  }
  client.sendMessage({
    to: phoneNumber,
    from: TWILIO_NUMBER,
    body
  }, function(err, responseData) {
    if (!err) {
      // http://www.twilio.com/docs/api/rest/sending-sms#example-1
      log('TW: SMS SENT TO', responseData.to);
    }
  });
}

function alertAdmin(problem) {
  if (DRY_MODE) {
    return log('DRY! ALERT ADMIN', problem);
  }
  client.sendMessage({
    to: ADMIN_NUMBER,
    from: TWILIO_NUMBER,
    body: problem
  }, function(err, responseData) {
    if (!err) {
      log('TW: alerted admin:', problem);
    }
  });
}


/**
 * Helpers
 */

function inStock(price) {
  const trimmed = price.trim().toLowerCase();
  return trimmed.length > 0 && trimmed !== 'sold out';
}

const COULD_NOT_FIND_SIZES = [];
const SOLD_OUT = [];

function refreshItems() {
  return getAcronymDotCom().then((items) => Promise.all(
    excudingFucked(items).map(item =>
      inStock(item.price) ?
        getProductPage(item.href)
          .then(
            (sizes) => ({...item, sizes}),
            (error) => ({...item, sizes: COULD_NOT_FIND_SIZES, error})
          ) :
      Object.assign({sizes: SOLD_OUT}, item))));
}

// returns an array of item names (as strings)
function getAcronymDotCom() {
  return get('https://acrnm.com/')
    .then(({data}) => parseIndex(data));
}

function getProductPage(productUrl) {
  return get(`https://acrnm.com${productUrl}`)
    .then(({data}) => parseProduct(data));
}

function now() {
  return (new Date()).toString();
}

function log(...message) {
  console.log(now(), ...message);
}

/**
 * Start polling
 */
function main() {
  const INITIAL_EMPTY_LIST = [];
  startServer();
  let itemsList = INITIAL_EMPTY_LIST;
  refreshItems().then(data => {
    itemsList = data;
    log('SERVER INITIALIZED');
    console.log(itemsToTable(itemsList));
    alertAdmin(
      `Alert server is up and has begin polling. ` +
      `Initialized with ${itemsList.length} items`);
  });
  log(`POLLING EVERY ${POLL_INTERVAL}ms`);
  setInterval(function () {
    log('POLLING...');
    refreshItems().then(function (data) {
      const newlyAdded = newItems(itemsList, data);
      const restock = newSizes(itemsList, data);
      if ((newlyAdded.length + restock.length) > 0) {
        log('FOUND NEW ITEMS');
        const report = prepareReport(itemsList, data);
        log(report);
        log(`Beginning notifications for ${ALERT_NUMBERS.join(', ')}.`);
        itemsList = data;
        const messageContent = report + '\nhttps://acrnm.com';
        ALERT_NUMBERS.forEach((number) => {
          sendSms(number, report);
          makeCall(number, newlyAdded.concat(restock).map(({name}) => name));
        });
        console.log(itemsToTable(itemsList));
      } else {
        log(`NO CHANGE: (${data.length})`);
      }
    }, (error) => {
      alertAdmin(`ERROR REFRESHING ACRNM. CHECK SERVER LOG`);
      log(error);
      error.stack && log(error.stack);
    });
  }, POLL_INTERVAL);
}

function itemsToTable(items) {
  try {
    const table = new Table({
      head: ['Name', 'Sizes']
    });
    items.forEach(({name, sizes}) =>
      table.push([
        name, sizes.length > 0 ? sizes.join(', ') : 'SOLD OUT'
      ]));

    return table.toString();
  }
  catch (e) {
    log('COULD NOT CONSTRUCT TABLE', e);
    return '';
  }
}

main();
