const fs = require('fs');

const cache = {};

module.exports = function get(url) {
  const requestNumber = cache[url] || 0;

  cache[url] = requestNumber + 1;

  return Promise.resolve({
    data: fs.readFileSync(`./fixtures/mock-responses/${urlToFile(url, requestNumber)}`, 'utf8')
  });
}

const PRODUCT_URL = 'https://acrnm.com/products/';
function urlToFile(url, number) {
  if (url === 'https://acrnm.com/') {
    return `index-${zeroPad(number)}.html`;
  }
  if (url.startsWith(PRODUCT_URL)) {
    return 'products-' +
      url.substr(PRODUCT_URL.length).toLowerCase() +
      '-' + zeroPad(number) + '.html';
  }
  throw new Error(`I do not know how to handle this url: "${url}"`);
}

function zeroPad(number) {
  return number < 10 ? `0${number}` : number;
}
