// Functions for comparing lists of items

function newItems(oldList, newList) {
  return newList.filter(({name}) =>
    oldList.every(item => item.name !== name));
}

function newSizes(oldList, newList) {
  return newList.reduce((diffItems, newItem) => {
    const oldItem = oldList.find(oldItem => oldItem.name === newItem.name);
    if (!oldItem) {
      return diffItems;
    }
    const newSizes = newItem.sizes.filter((newSize) => !oldItem.sizes.includes(newSize));
    if (newSizes.length === 0) {
      return diffItems;
    }
    return diffItems.concat({
      name: newItem.name,
      sizes: newSizes
    });
  }, []);
}

function prepareReport(oldList, newList) {
  const theseNewItems = newItems(oldList, newList);
  const theseNewSizes = newSizes(oldList, newList);

  return (theseNewItems.length > 0 ?
    ['NEW:'].concat(theseNewItems.map(itemSummary)) : [])
    .concat(theseNewSizes.length > 0 ?
      ['RESTOCK:'].concat(theseNewSizes.map(itemSummary)) :
      []
    ).join('\n');
}

function itemSummary({name, sizes}) {
  return `${name}: ${sizeSummary(sizes)}`;
}

function sizeSummary(sizes) {
  return sizes.length > 3 ? `${sizes.length} Sizes` : sizes.join(', ');
}

module.exports = {prepareReport, newItems, newSizes};
